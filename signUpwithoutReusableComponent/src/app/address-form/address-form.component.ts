import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})
export class AddressFormComponent implements OnInit {

  addressForm = new FormGroup({
       line1 : new FormControl(''),
       line2  : new FormControl(''),
       city : new FormControl(''),
       state : new FormControl(''),
       country: new FormControl(''),
       zip: new FormControl('')
       });

  constructor() { }

  ngOnInit() {
  }

}
