import { Component, OnInit , OnChanges , SimpleChanges, AfterViewInit , ViewChild} from '@angular/core';
import {FormControl, FormGroup , FormBuilder, FormArray, NgForm} from '@angular/forms';
import { Address} from '../address';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit    {

profileForm: FormGroup;
  //  private count = 1 ;
  //  phoneNumberIds: number[] = [1] ;
  selectedOther = true;
  sameAddress = false;
// shippingAddress: Address = new Address();
 // billingAddress: Address = new Address();
  phoneNumberType = ['landline', 'mobile', 'fax'];
   receiveUpdates = [
     {name: 'SMS' ,  value: 'sms'},
     {name: 'EMAIL' , value: 'email'},
    {name: 'WHATSAPP', value: 'whatsApp'}];

    otherUserType() {
      this.selectedOther = !this.selectedOther ;
      }
      /* if radio button is selected instead of ither userType*/
    radioUserType() {
      this.selectedOther = true ;
    }

  sameAddressFor() {
    this.sameAddress = !this.sameAddress;
    if (this.sameAddress) {
      this.profileForm.value.billingAddress.billingAddressLine1 = this.profileForm.value.shippingAddress.shippingAddressLine1 ;
      this.profileForm.value.billingAddress.billingAddressLine2 = this.profileForm.value.shippingAddress.shippingAddressLine2 ;
      this.profileForm.value.billingAddress.billingAddressCountry = this.profileForm.value.shippingAddress.shippingAddressCountry ;
      this.profileForm.value.billingAddress.billingAddressCity = this.profileForm.value.shippingAddress.shippingAddressCity ;
      this.profileForm.value.billingAddress.billingAddressState = this.profileForm.value.shippingAddress.shippingAddressState ;
      this.profileForm.value.billingAddress.billingAddressZip = this.profileForm.value.shippingAddress.shippingAddressZip ;
      this.profileForm.get('billingAddress').patchValue( {
      billingAddressLine1 : this.profileForm.get('shippingAddress').value.shippingAddressLine1,
      billingAddressLine2 : this.profileForm.get('shippingAddress').value.shippingAddressLine2 ,
      billingAddressCountry : this.profileForm.get('shippingAddress').value.shippingAddressCountry,
      billingAddressCity : this.profileForm.get('shippingAddress').value.shippingAddressCity,
      billingAddressState : this.profileForm.get('shippingAddress').value.shippingAddressState,
      billingAddressZip : this.profileForm.get('shippingAddress').value.shippingAddresszip,
      });
    }


}



  onSubmit() {
  console.log(this.profileForm.value);
  const user = {
      firstName : this.profileForm.value.firstName,
      lastName : this.profileForm.value.lastName,
      email : this.profileForm.value.email,
      password : this.profileForm.value.password,
      confirmPassword : this.profileForm.value.confirmPassword,
      userType : this.profileForm.value.userType,
      receiveUpdates1 : this.profileForm.value.updates

  };
  console.log(user);
  }




//   remove(i: number) {
//    this.phoneNumberIds. splice(i , 1);
//  }
//   add() {
//     this.phoneNumberIds.push(++this.count);
//   }





constructor(private fb: FormBuilder) {


}

onCheckboxChange($event) {
  const updates: FormArray = this.profileForm.get('updates') as FormArray;
  if ($event.target.checked) {
    updates.push(new FormControl($event.target.value));
  } else {
    let  i = 0;
    updates.controls.forEach((item: FormControl) => {
    if (item.value === $event.target.value) {
      updates.removeAt(i);

    }
    i++;
  });
  }
}

ngOnInit() {
  this.profileForm = this.fb.group({
    firstName : new FormControl(''),
    lastName  : new FormControl(''),
    email : new FormControl(''),
    password : new FormControl(''),
    confirmPassword: new FormControl(''),
    userType: new FormControl(''),
   /* phoneNumber : new FormArray([
      new FormGroup({
        phone: new FormControl(''),
        type: new FormControl('')
      })
    ]),*/
    shippingAddress : this.fb.group({
      shippingAddressLine1: new FormControl(''),
      shippingAddressLine2: new FormControl(''),
      shippingAddressCity: new FormControl(''),
      shippingAddressState:  new FormControl(''),
      shippingAddressCountry:  new FormControl(''),
      shippingAddressZip: new FormControl('')
    }),
    billingAddress: this.fb.group({
      billingAddressLine1 : new FormControl(''),
      billingAddressLine2 : new FormControl(''),
      billingAddressCity : new FormControl(''),
      billingAddressState : new FormControl(''),
      billingAddressCountry : new FormControl(''),
      billingAddressZip : new FormControl(''),
    }),
    updates: new FormArray([])

   });




}



}


