import { Component, OnInit , AfterViewInit, Output, EventEmitter, OnChanges, SimpleChanges, Input} from '@angular/core';
import {FormControl, FormGroup, AbstractControl, FormBuilder} from '@angular/forms';
import { Address} from '../address' ;
import {zipCodeValidator} from '../customValidators/zipCodeValidator';

@Component({
  selector: 'app-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.css']
})
export class AddressFormComponent implements OnInit , AfterViewInit, OnChanges  {
  addressForm: FormGroup ;

 @Output() address = new EventEmitter();
 @Input() addressObj: Address ;
 @Input() update: boolean;
  constructor(private fb: FormBuilder) {
  this.addressForm = this.fb.group({
    line1 : [''],
    line2  : [''],
    city : [''],
    state : [''],
    country: [''],
    zip: ['',[zipCodeValidator()]]
    });
  }


  ngOnInit() {

  }
  get f() { return this.addressForm.controls; }

  ngOnChanges(change: SimpleChanges) {


  this.addressForm.setValue({
      line1: this.addressObj.line1,
      line2: this.addressObj.line2,
      city: this.addressObj.city,
      state: this.addressObj.state,
      country: this.addressObj.country,
      zip: this.addressObj.zip
    });

  }
  ngAfterViewInit()  {
    this.addressForm.valueChanges.subscribe(() => {

       this.addressObj.line1 = this.addressForm.getRawValue().line1;
       this.addressObj.line2 = this.addressForm.getRawValue().line2;
       this.addressObj.city =  this.addressForm.getRawValue().city;
       this.addressObj.state = this.addressForm.getRawValue().state;
       this.addressObj.country = this.addressForm.getRawValue().country;
       this.addressObj.zip = this.addressForm.getRawValue().zip;
       this.address.emit(this.addressObj);

      });
      }


}
