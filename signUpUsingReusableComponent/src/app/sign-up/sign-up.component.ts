import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup , FormBuilder, FormArray, Validators} from '@angular/forms';
import { Address} from '../address';
import {phoneValidator} from '../customValidators/PhoneValidator ';
import {passwordValidator} from '../customValidators/passwordValidator';
import {confirmPasswordValidator} from '../customValidators/confirmPasswordValidator';




@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit    {

profileForm: FormGroup;
myAddress = new Address();
shippingAddress: Address = new Address();
billingAddress: Address = new Address();
phoneNumberType = ['landline', 'mobile', 'fax'];
receiveUpdates = [
                  {name: 'SMS' ,  value: 'sms'},
                  {name: 'EMAIL' , value: 'email'},
                  {name: 'WHATSAPP', value: 'whatsApp'}];
selectedOther = true;
sameAddress = false;
submitted = false;

otherUserType() {
    this.selectedOther = !this.selectedOther ;
      }
   /* if radio button is selected instead of ither userType*/
radioUserType() {
   this.selectedOther = true ;
    }


get f() { return this.profileForm.controls; }

phoneNumbersAry() {
  return this.profileForm.get('phoneNumbers') as FormArray;
}


addPhoneNumber() {
  const phoneNumbers: FormArray = this.profileForm.get('phoneNumbers') as FormArray ;
  phoneNumbers.push( this.fb.group({
     phone: new FormControl(''), type : new FormControl('')
  }));
}
  removePhoneNumber(i: number) {
    const phoneNumbers: FormArray = this.profileForm.get('phoneNumbers') as FormArray ;
    phoneNumbers.removeAt(i);

}



onCheckboxChange($event) {
  const updates: FormArray = this.profileForm.get('updates') as FormArray;
  if ($event.target.checked) {
    updates.push(new FormControl($event.target.value));
  } else {
    let  i = 0;
    updates.controls.forEach((item: FormControl) => {
    if (item.value === $event.target.value) {
      updates.removeAt(i);

    }
    i++;
  });
  }
}
constructor(private fb: FormBuilder) {

}

ngOnInit() {
  this.profileForm = this.fb.group({
    firstName : ['', [Validators.required]],
    lastName  : ['', [Validators.required]],

    email : ['', [Validators.required,
                                Validators.email]],
    password : ['', [Validators.required, passwordValidator()]],
    confirmPassword: ['', [Validators.required]],
    userType: ['', [Validators.required]],
   phoneNumbers : this.fb.array([
      this.fb.group({
        phone: ['', [Validators.required, phoneValidator()]],
        type: ['']
      })
    ]),
    updates: new FormArray([])

   },
   { validator: confirmPasswordValidator()});


}



sameAddressFor() {
  this.sameAddress = !this.sameAddress;
  if (this.sameAddress) {

    this.billingAddress.line1 = this.shippingAddress.line1 ;
    this.billingAddress.line2 = this.shippingAddress.line2 ;
    this.billingAddress.city = this.shippingAddress.city ;
    this.billingAddress.state = this.shippingAddress.state ;
    this.billingAddress.country = this.shippingAddress.country ;
    this.billingAddress.zip = this.shippingAddress.zip ;


  }


}
getShippingAddress($event) {
this.myAddress = $event ;
console.log(this.myAddress);
this.shippingAddress.line1 = this.myAddress.line1;
this.shippingAddress.line2 = this.myAddress.line2;
this.shippingAddress.city = this.myAddress.city;
this.shippingAddress.state = this.myAddress.state;
this.shippingAddress.country = this.myAddress.country;
this.shippingAddress.zip = this.myAddress.zip;
console.log('getShippingAddress:' , this.shippingAddress);
}
getBillingAddress($event) {
this.myAddress = $event ;
console.log(this.myAddress);
this.billingAddress.line1 = this.myAddress.line1;
this.billingAddress.line2 = this.myAddress.line2;
this.billingAddress.city = this.myAddress.city;
this.billingAddress.state = this.myAddress.state;
this.billingAddress.country = this.myAddress.country;
this.billingAddress.zip = this.myAddress.zip;
console.log('getbillingAddress:' , this.billingAddress);

}

onSubmit() {
  this.submitted = true;

  // stop here if form is invalid
  if (this.profileForm.invalid) {
      return;
  }
  const user = {
    firstName : this.profileForm.value.firstName,
    lastName : this.profileForm.value.lastName,
    email : this.profileForm.value.email,
    password : this.profileForm.value.password,
    confirmPassword : this.profileForm.value.confirmPassword,
    phoneNumbers: this.profileForm.value.phoneNumbers,
    userType : this.profileForm.value.userType,
    ShippingAddress: this.shippingAddress,
    BillingAddress: this.billingAddress,
    receiveUpdates1 : this.profileForm.value.updates

};
  console.log(user);

}
onReset() {
  this.submitted = false;
  this.profileForm.value.reset();
}



}


