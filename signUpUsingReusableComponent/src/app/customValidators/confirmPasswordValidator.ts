import {AbstractControl, ValidatorFn, FormGroup} from '@angular/forms';


export function confirmPasswordValidator(): ValidatorFn {
  return(control: AbstractControl): {[key: string]: boolean}| null => {
    const password = control.get('password').value;
    const confirmPassword = control.get('confirmPassword').value;


    if (password !== confirmPassword ) {


        return {passwordMissMatch : true};

    }
    return null;
  };

}

