import {AbstractControl, ValidatorFn} from '@angular/forms';


export function phoneValidator(): ValidatorFn {
  return(control: AbstractControl): {[key: string]: boolean}| null => {
    if (control.value.length !== 11 ) {
      return{InvalidPhoneNumber : true};
    }
    return null;
  };

}
