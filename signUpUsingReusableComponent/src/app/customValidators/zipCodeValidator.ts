import {AbstractControl, ValidatorFn} from '@angular/forms';


export function zipCodeValidator(): ValidatorFn {
  return(control: AbstractControl): {[key: string]: boolean}| null => {
    if (control.value.length !== 7 ) {
      return{InvalidZipCode : true};
    }
    return null;
  };

}
