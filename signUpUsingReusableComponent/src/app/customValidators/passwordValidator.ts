import {AbstractControl, ValidatorFn} from '@angular/forms';


export function passwordValidator(): ValidatorFn {
  return(control: AbstractControl): {[key: string]: boolean}| null => {
    if (control.value.length > 3 && control.value.length < 6) {
      return {weekPassword : true};

    }
    if (control.value.length > 7 && control.value.length < 10) {
      return {strongPassword: true};

    }



    return null;
  };

}
